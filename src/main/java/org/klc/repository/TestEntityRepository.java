package org.klc.repository;

import org.klc.entity.TestEntity;
import org.springframework.data.repository.CrudRepository;

public abstract interface TestEntityRepository extends CrudRepository<TestEntity, Long> {
}
