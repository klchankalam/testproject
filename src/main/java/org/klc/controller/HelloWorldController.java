package org.klc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloWorldController {
	private static final Logger logger = LoggerFactory.getLogger(HelloWorldController.class);

	@RequestMapping({ "/helloWorld" })
	public ModelAndView helloWorld() {
		logger.debug("This is debug msg for testing.");
		ModelAndView mav = new ModelAndView();
		mav.addObject("msg", "Hello World!");
		mav.setViewName("test1");
		return mav;
	}
}
