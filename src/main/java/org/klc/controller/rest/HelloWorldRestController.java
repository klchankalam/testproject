package org.klc.controller.rest;

import java.sql.Timestamp;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.klc.entity.TestEntity;
import org.klc.service.TestEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldRestController {

	@Autowired
	TestEntityService service;

	@RequestMapping({ "/rest/testEntity" })
	public Iterable<TestEntity> test() {
		TestEntity te = new TestEntity();
		te.creationTime = new Timestamp(System.currentTimeMillis());
		te.title = "test title";

		service.saveTestEntity(te);

		return StreamSupport.stream(service.getAllTestEntity().spliterator(), false).filter(u -> u.title.length() > 1)
				.collect(Collectors.toList());
	}
}
