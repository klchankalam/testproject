package org.klc.config;

import java.io.IOException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.klc.security.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.jta.JtaTransactionManager;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import freemarker.template.TemplateException;

@Configuration
@ComponentScan(basePackages={"org.klc"})
@EnableWebMvc
@EnableJpaRepositories(basePackages={"org.klc.repository"})
@EnableTransactionManagement
@Import({SecurityConfig.class})
@PropertySource({"classpath:TestApp.properties"})
public class TestAppConfig
  extends WebMvcConfigurerAdapter
{
  @Autowired
  Environment env;
  
  public void configureViewResolvers(ViewResolverRegistry registry)
  {
    registry.freeMarker().cache(false);
  }
  
  @Bean
  public FreeMarkerConfigurer freeMarkerConfigurer()
    throws TemplateException, IOException
  {
    FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
    configurer.setTemplateLoaderPath("/WEB-INF/freemarker/");
    return configurer;
  }
  
  @Bean
  public AbstractPlatformTransactionManager transactionManager()
  {
    Properties p = new Properties();
    p.put("transactionManagerName", "java:/TransactionManager");
    JtaTransactionManager txMgr = new JtaTransactionManager();
    txMgr.setJndiEnvironment(p);
    return txMgr;
  }
  
  @Bean
  public EntityManagerFactory entityManagerFactory()
  {
    LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
    bean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
    bean.setJtaDataSource(dataSource());
    Properties props = new Properties();
    props.setProperty("hibernate.dialect", this.env.getProperty("hibernate.dialect"));
    props.setProperty("hibernate.hbm2ddl.auto", this.env.getProperty("hibernate.hbm2ddl.auto"));
    props.setProperty("hibernate.show_sql", this.env.getProperty("hibernate.show_sql"));
    props.setProperty("hibernate.format_sql", this.env.getProperty("hibernate.format_sql"));
    props.setProperty("hibernate.transaction.jta.platform", this.env.getProperty("hibernate.transaction.jta.platform"));
    bean.setJpaProperties(props);
    bean.setPersistenceUnitName("testJPA");
    bean.setPackagesToScan(new String[] { "org.klc.entity" });
    bean.afterPropertiesSet();
    return bean.getObject();
  }
  
  @Bean
  public DataSource dataSource()
  {
    JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
    dsLookup.setResourceRef(true);
    DataSource dataSource = dsLookup.getDataSource(this.env.getProperty("database.jndi"));
    return dataSource;
  }
}
