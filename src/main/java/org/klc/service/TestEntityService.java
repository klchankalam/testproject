package org.klc.service;

import org.klc.entity.TestEntity;
import org.klc.repository.TestEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestEntityService {
	@Autowired
	TestEntityRepository teRepo;

	public void saveTestEntity(TestEntity te) {
		this.teRepo.save(te);
	}

	public Iterable<TestEntity> getAllTestEntity() {
		return this.teRepo.findAll();
	}
}
