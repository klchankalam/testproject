package org.klc.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "test_entity")
public class TestEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ", timezone = "Asia/Hong_Kong")
	@Column(name = "creation_time", nullable = false)
	public Timestamp creationTime;
	
	@Column(name = "description", length = 500)
	public String description;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ", timezone = "Asia/Hong_Kong")
	@Column(name = "modification_time")
	public Timestamp modificationTime;
	
	@Column(name = "title", nullable = false, length = 100)
	public String title;
	
	@Version
	public long version;
}
