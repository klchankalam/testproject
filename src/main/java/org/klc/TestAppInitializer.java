package org.klc;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.klc.config.TestAppConfig;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class TestAppInitializer implements WebApplicationInitializer {
	public void onStartup(ServletContext container) {
		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
		ctx.register(new Class[] { TestAppConfig.class });
		ctx.setServletContext(container);
		ServletRegistration.Dynamic registration = container.addServlet("dispatcher", new DispatcherServlet(ctx));
		registration.setLoadOnStartup(1);
		registration.addMapping(new String[] { "/*" });
	}
}