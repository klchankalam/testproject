# TestProject
Test setup for:
- jboss 9.0.1
- spring 4.2
- spring data
- spring security
- hibernate 5.0.0
- freemarker

compiled with Java 8

#Test for:
- Simple login page
- Simple page with template
- REST with entity persist and loading
- maven dependencies

#Setup for jboss
- in admin console add datasource with jndi, and deploy postgres jdbc41 jar like war file
- log level controled in admin console as well
